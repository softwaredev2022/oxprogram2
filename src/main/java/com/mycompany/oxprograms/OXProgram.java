/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.oxprograms;

import java.util.Scanner;
import java.util.Set;

/**
 *
 * @author a
 */
public class OXProgram {

    static char table[][] = {{'-', '-', '-'}, {'-', '-', '-'}, {'-', '-', '-'}};
    static char currentPlayer = 'O';
    static int row, col;
    static Scanner n = new Scanner(System.in);
    static boolean finish = false;
    static int count = 0;

    public static void main(String[] args) {

        shpwWelcome();
        while (true){
            showTable();
            showTurn();
            intputRowCol();
            process();
            if(finish){
                break;
            }
        }
    }

    public static void showTable() {
        for (int ro = 0; ro < table.length; ro++) {
            for (int co = 0; co < table[ro].length; co++) {
                System.out.print(table[ro][co]);
            }
            System.out.println("");
        }
    }

    public static void shpwWelcome() {
        System.out.println("Welcome to OX Game");

    }

    public static void showTurn() {
        System.out.println("Turn " + currentPlayer);
    }

    public static void intputRowCol() {
        System.out.println("Please input row, col");
        row = n.nextInt();
        col = n.nextInt();
    }

    public static void process() {
        if (setTable()) {
            if(checkWin()){
                finish = true;
                showTable();
                System.out.println(">>>"+ currentPlayer + " Win<<<");
                return;
            }
            if(count == 8){
                finish = true;
                showTable();
                showDraw();
                return;
            }
            count++;
            switchPlayer();
        }

    }

    public static void switchPlayer() {
        if (currentPlayer == 'O') {
            currentPlayer = 'X';
        } else {
            currentPlayer = 'O';
        }
    }

    public static boolean setTable() {
        table[row - 1][col - 1] = currentPlayer;
        return true;
    }

    public static boolean checkWin() {
        if(checkVertical()){
            return true;
        }else if(checkHorizontal()){
            return true;
        }else if(checkX()){
            return true;
        }
        return false;
    }

    public static boolean checkVertical() {
        for (int r = 0; r < table.length; r++) {
            if(table[r][col-1]!=currentPlayer){
                return false;
            }
        }
         return true;
    }

    public static boolean checkHorizontal() {
        for (int c = 0; c < table.length; c++) {
            if(table[c][row-1]!=currentPlayer){
                return false;
            }
        }
         return true;
    }

    public static boolean checkX() {
        if(checkX1()){
            return true;
        }else if(checkX2()){
            return true;
        }
        return false;
    }

    public static boolean checkX1() {
        for(int i=0; i<table.length; i++){
            if(table[i][i]!=currentPlayer){
                return false;
            }
        }
        return true;
    }

    public static boolean checkX2() {
        for(int i=0; i<table.length; i++){
            if(table[i][2-i]!=currentPlayer){
                return false;
            }
        }
        return false;
    }

    public static void showDraw() {
        System.out.println(">>>Draw<<<");
    }

}
